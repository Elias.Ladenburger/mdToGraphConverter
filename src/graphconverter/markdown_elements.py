from enum import Enum

class MDTYPE(str, Enum):
    def __new__(cls, value, identifier, rank):
        mdtype = str.__new__(cls, value)
        mdtype._value_ = value
        mdtype.identifier = identifier
        mdtype.rank = rank
        return mdtype

    DOCUMENT = ("Document", "", 0)
    HEADER = ("Header", "#", 1)
    ELEMENT = ("Element", "", 2)
    LISTITEM = ("List Item", "*", 2)
    TABLE = ("Table", "", 2)
    PARAGRAPH = ("Paragraph", "", 2)


class MDRELATIONSHIPTYPE(str, Enum):
    PARENT = "contains"
    CHILD = "belongs to"
    REFERENCE = "connects to"


class MDElement:
    def __init__(self, content: str = "", type: MDTYPE = None, parent=None):
        self.content = content
        self._type = type or self._assess_type(content)
        self.parent = parent

    @property
    def title(self):
        return self.content.replace(self.type.identifier, "")

    @property
    def type(self):
        return self._type

    def _assess_type(self, content: str):
        if not content:
            return MDTYPE.PARAGRAPH
        elif content.startswith("\t") or content.startswith(" "):
            content = content[1:]
            return self._assess_type(content)
        elif content.startswith("#"):
            return MDTYPE.HEADER
        elif content.startswith("*") or content.startswith("-") or content[0].isnumeric():
            return MDTYPE.LISTITEM
        elif content.startswith("|"):
            return MDTYPE.TABLE
        else:
            return MDTYPE.PARAGRAPH


    def __str__(self):
        return_str = "title: " + str(self.title) + "\n"
        return_str += "content: " + str(self.content) + "\n"
        return_str += "type: " + str(self.type) + "\n"
        return return_str

    def __eq__(self, other):
        return str(self) == str(other)


class MDRelation:
    def __int__(self, from_elem: MDElement, relationship: MDRELATIONSHIPTYPE, to_elem: MDElement):
        self.from_elem = from_elem
        self.relationship = relationship
        self.to_elem = to_elem