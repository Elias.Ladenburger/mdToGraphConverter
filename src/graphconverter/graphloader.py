import os
import io
from xml.dom import WrongDocumentErr
import markdown


class MDLoader:
    """Reponsible for providing legal markdown."""
    
    @staticmethod
    def parse_markdown(source, to_html=False):
        """
        This function will parse correct markdown from a source and return in a pre-defined output format.
        """
        my_result = MDLoader._load_markdown(source)
        if to_html:
            extensions = ['sane_lists', 'extra']
            my_result = markdown.markdown(my_result, output_format="xhtml",
                                          extensions=extensions)
        return my_result

    @staticmethod
    def _load_markdown(source):
        my_md = ""
        if isinstance(source, io.IOBase):
            my_md = source.read()
        if isinstance(source, str):
            if os.path.isfile(source):
                if source.endswith(".md"):
                    with open(source) as mdfile:
                        my_md = mdfile.read()
                else:
                    raise WrongDocumentErr("This file is not a markdown file!")
            else:
                my_md = source
        return my_md

