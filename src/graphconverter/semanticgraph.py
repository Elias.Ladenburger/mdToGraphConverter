import json
from tkinter.font import names

import rdflib
from rdflib import URIRef, BNode, Namespace
from rdflib.namespace import RDFS, RDF
from rdflib.plugins.sparql import prepareQuery

schema_NS = Namespace("http://md2rdf.eliasladenburger.com/schema#")
schemastr = str(schema_NS)

class SemanticReasoner:

    def __init__(self, graph, schema=None):
        self.graph = graph
        if schema:
            self.graph.merge_graphs(schema)

    def perform_reasoning(self):
        self._reason_ranges()
        self._reason_domain()
        self._reason_subproperties()
        self._reason_subclasses()
        self._reason_additional_properties()
        return self.graph

    def _reason_ranges(self):
        # interesting to implement but is not strictly required for the experiment, as of now
        pass

    def _reason_domain(self):
        # interesting to implement but is not strictly required for the experiment, as of now
        pass

    def _reason_subproperties(self):
        """
        This method looks for triples s, p, o where p is "rdfs:subPropertyOf" another property.
        It will then insert a triple s, superproperty, o, if it doesn't already exist.

        When this method has changed the graph, it will recursively call itself until the knowledge graph remains unchanged.
        """
        screenshot_graph = self.graph
        subProp = RDFS.term("subPropertyOf")
        for subproperty, is_subproperty, superproperty in self.graph.triples((None, RDFS.subPropertyOf, None)):
            for subject, occurrence_of_sub, value in self.graph.triples((None, subproperty, None)):
                already_analyzed = rdflib.Graph().value(subject, superproperty, None)
                if already_analyzed is None:
                    self.graph.add((subject, superproperty, value))

        if screenshot_graph.serialize() != self.graph.serialize():
            self._reason_subproperties()

    def _reason_subclasses(self):
        """
        This method looks for triples s, p, o where s is "rdfs:subClassOf" a superclass.
        It will then all properties of the superclass to s, if they don't already exist.

        When this method has changed the graph, it will recursively call itself until the knowledge graph remains unchanged.
        """
        screenshot_graph = self.graph
        for subclass, is_subclass, superclass in self.graph.triples((None, RDFS.subClassOf, None)):
            for occurrence_of_super, prop, value in self.graph.triples((superclass, None, None)):
                already_analyzed = rdflib.Graph().value(subject=subclass, predicate=prop, object=None) == value
                if not already_analyzed:
                    self.graph.add((subclass, prop, value))

        if screenshot_graph.serialize() != self.graph.serialize():
            self._reason_subclasses()

    def _reason_additional_properties(self):
        snapshot_graph = self.graph
        query = prepareQuery(queryString="""CONSTRUCT { 
                                            ?subject ?property ?value 
                                            } 
                                             WHERE { 
                                             ?subject a ?schematype . 
                                             ?schematype ?property ?value .
                                             FILTER(STRSTARTS(STR(?schematype), \" """ + schemastr + """\"))
                                             } 
                                             """)
        query_result = self.graph.query(query).graph
        datastring = query_result.serialize().decode(encoding="ascii")
        self.graph.parse(data=datastring)

        if self.graph != snapshot_graph:
            self._reason_additional_properties()


class SemanticMarkdownElement:
    """
    A markdown element represented as an RDF triplet.
    """
    def __init__(self, namespace: str, content: str, type: str, title: str = "", parent = None):
        self.ns = self._parse_namespace(namespace)
        self.content = content
        self.type = type
        self.title = title or content[0:14]
        self.parent = parent
        self.elem_graph = self._convert_rdf()

    @property
    def rdf(self):
        return self.elem_graph


    def _convert_rdf(self):
        elem_graph = SemanticGraph()
        master_node = BNode()
        elem_graph.add((master_node, RDFS.label, self.title))
        elem_graph.add((master_node, schema_NS.content, self.content))
        # elem_graph.add((master_node, RDF.type, self.type))
        elem_graph.add((master_node, schema_NS.belongs_to, self.parent))


class SemanticGraph(rdflib.Graph):
    """
    A wrapper class for the rdflib.Graph() class so that we can introduce convenience functions and reduce overall coupling
    """
    def __init__(self):
        super().__init__()

    def merge_graph(self, other_graph):
        """
        Merges another SemanticGraph into this graph
        """
        if isinstance(other_graph, SemanticGraph):
            data = other_graph.serialize_to_json()
        elif isinstance(other_graph, rdflib.Graph):
            data = other_graph.serialize(format="json-ld").decode(encoding="utf-8")
        else:
            raise NotImplemented
        self.parse(data=data, format="json-ld")

    def to_dict(self):
        return json.loads(self.serialize_to_json())

    def serialize_to_json(self):
        """Returns a json-ld serialization as string of all the rdf triplets in this graph."""
        context = self.convert_namespace()
        return self.serialize(format="json-ld", indent=2, context=context, use_native_types=True).decode(encoding="utf-8")

    def serialize_as_n3(self):
        """Returns a ttl serialization as string of all the rdf triplets in this graph."""
        return self.serialize(format="n3").decode(encoding="utf-8")

    def convert_namespace(self):
        ns_dict = {}
        for prefix, namespace in self.namespaces():
            ns_dict[prefix] = namespace.lower()
        return ns_dict

    def get_node_name(self, node):
        for ns in self.convert_namespace():
            if isinstance(node, rdflib.Literal):
                return node.lower()
            else:
                node_name = str.replace(old=node.lower(), new="")
                if node != node_name:
                    return node_name
        return node_name
