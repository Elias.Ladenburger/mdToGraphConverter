import markdown
import html

from graphconverter.graphloader import MDLoader
from graphconverter.semanticgraph import SemanticGraph
from graphconverter.markdown_elements import MDElement, MDRelation, MDTYPE, MDRELATIONSHIPTYPE

# Iterate through a markdown document:
#    if you come across a structuring character, create a new relationship:
        # name this relationship with whatever is on the rest of the line

        # create a new node
        #     add to this node all the content below the structuring char
        #     call this algorithm on the node

class MDtoGraphConverter:
    """
    params: 
    """
    def __init__(self, src, title:str=""):
        self.my_md = MDLoader.parse_markdown(src)
        if title:
            self.title = title
        else:
            title = src.split("/")
            self.title = src[-1]

    def convert(self):
        """
        Converts a markdown string into a graph.
        """
        md_lines = self.my_md.split("\n")

        elems = []
        prev_elem = MDElement(content=self.title, type=MDTYPE.DOCUMENT)

        for cur_line in md_lines:
            elem = MDElement(content=cur_line)
            elem = self._evaluate_element(elem, prev_elem)
            elems.append(elem)
            prev_elem = elem
        return elems

    def _evaluate_element(self, elem: MDElement, previous_element: MDElement) -> MDElement:
        if not previous_element:
            return elem

        if elem.type.rank == previous_element.type.rank:
            elem = self._evaluate_type(elem, previous_element)
        elif elem.type.rank > previous_element.type.rank:
            elem.parent = previous_element
        elif elem.type.rank < previous_element.type.rank:
            elem = self._evaluate_element(elem=elem, previous_element=previous_element.parent)
        return elem


    def _evaluate_type(self, elem: MDElement, prev_elem: MDElement):
        if elem.type == prev_elem.parent.type:
            elem.parent = prev_elem.parent
        else:
            elem.parent = prev_elem
        return elem

    def _convert_toggle(self, node):
        pass


class HTMLtoRDFconverter:
    def __init__(self):
        pass

    def convert(self):
        pass