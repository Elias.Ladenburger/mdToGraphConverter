import unittest

from graphconverter.markdown_elements import MDElement, MDTYPES

class MyTestCase(unittest.TestCase):
    def test_assert_equality_working(self):
        parent_1 = MDElement(title="My MD document", type=MDTYPES.DOCUMENT)
        parent_2 = MDElement(title="My MD document", type=MDTYPES.DOCUMENT)
        self.assertEqual(parent_1, parent_2)

    def test_assert_equality_with_parent_working(self):
        parent_1 = MDElement(title="My MD document", type=MDTYPES.DOCUMENT)
        parent_2 = MDElement(title="My MD document", type=MDTYPES.DOCUMENT)
        first_element = MDElement(title="About", content="# About", parent=parent_1, type=MDTYPES.HEADER)
        second_element = MDElement(title="About", content="# About", parent=parent_2, type=MDTYPES.HEADER)
        self.assertEqual(first_element, second_element)

    def test_assert_equality_different_title(self):
        parent_1 = MDElement(title="My MD document", type=MDTYPES.DOCUMENT)
        parent_2 = MDElement(title="My MD document", type=MDTYPES.DOCUMENT)
        first_element = MDElement(title="About", content="# About", parent=parent_1, type=MDTYPES.HEADER)
        second_element = MDElement(title="Other", content="# About", parent=parent_2, type=MDTYPES.HEADER)
        self.assertNotEqual(first_element, second_element)

    def test_assert_equality_different_content(self):
        parent_1 = MDElement(title="My MD document", type=MDTYPES.DOCUMENT)
        parent_2 = MDElement(title="My MD document", type=MDTYPES.DOCUMENT)
        first_element = MDElement(title="About", content="# About", parent=parent_1, type=MDTYPES.HEADER)
        second_element = MDElement(title="About", content="# Other", parent=parent_2, type=MDTYPES.HEADER)
        self.assertNotEqual(first_element, second_element)

    def test_assert_equality_different_parent(self):
        parent_1 = MDElement(title="My MD document", type=MDTYPES.DOCUMENT)
        parent_2 = MDElement(title="My MD document", type=MDTYPES.DOCUMENT)
        first_element = MDElement(title="About", content="# About", parent=parent_1, type=MDTYPES.HEADER)
        second_element = MDElement(title="About", content="# About", parent=None, type=MDTYPES.HEADER)
        self.assertNotEqual(first_element, second_element)

    def test_assert_equality_different_type(self):
        parent_1 = MDElement(title="My MD document", type=MDTYPES.DOCUMENT)
        parent_2 = MDElement(title="My MD document", type=MDTYPES.DOCUMENT)
        first_element = MDElement(title="About", content="# About", parent=parent_1, type=MDTYPES.HEADER)
        second_element = MDElement(title="About", content="# About", parent=parent_2, type=MDTYPES.PARAGRAPH)
        self.assertNotEqual(first_element, second_element)

if __name__ == '__main__':
    unittest.main()
