import sys
import os

# platform-agnostic import from parent directory
cur_dir = os.path.dirname(os.path.realpath(__file__))
parent_dir = os.path.dirname(cur_dir)
sys.path.append(parent_dir)

import unittest
import rdflib

from graphconverter.graphloader import MDLoader
from graphconverter.graphconverter import MDtoGraphConverter
from graphconverter.markdown_elements import MDElement, MDTYPE

md_file = "D:\Projects\MDtoGraph\src\\tests\\resources\\test_readme.md"


class MDConverterTest(unittest.TestCase):
    def setUp(self):
        return super().setUp()

    def tearDown(self) -> None:
        return super().tearDown()

    def test_load_illegal_filename(self):
        file_path = "D:\Projects\MDtoGraph\src\\tests\\resources\\test_readme.xml"
        markdown = MDLoader.parse_markdown(file_path)
        self.assertTrue(True)

    def test_load__legal_filename(self):
        file_path = "D:\Projects\MDtoGraph\src\\tests\\resources\\test_readme.md"
        markdown = MDLoader.parse_markdown(file_path)
        self.assertTrue(True)

    def test_convert_all(self):
        my_converter = MDtoGraphConverter(md_file)
        elems = my_converter.convert()
        for elem in elems:
            print(elem.content)
        self.assertIsInstance(elems[0], MDElement)

    def test_load_mdstring(self):
        self.assertTrue(True)

    def test_convert_bullet(self):
        pass

    def test_loading_md_html(self):
        loader = MDLoader
        html = MDLoader.parse_markdown(md_file, to_html=True)
        print(html)

    def test_convert_heading(self):
        converter = MDtoGraphConverter("# About")
        new_elem = converter.convert()
        compare_elem = MDElement(type=MDTYPE.HEADER, content="# About")
        print(new_elem[0])
        print(compare_elem)
        self.assertEqual(new_elem[0], compare_elem)


if __name__ == '__main__':
    unittest.main()
