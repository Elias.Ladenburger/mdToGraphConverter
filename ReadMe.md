# About

This project will allow transforming any markdown (.md) file into a (visual) graph (currently using the RDF format).
It is still in progress.

## How to use

Clone this repository with
  
    git clone {link-to-this-repository}
    
Move into the newly created directory

    cd {project-name}

Set up your virtual environment:
    
    python3 -m venv my-venv
    # Windows
    # my-venv\Scripts\activate.bat 
    #
    # Unix (MacOS, Linux)
    # source my-venv/bin/activate
    
In your venv environment run:

    python3 -m pip install -r requirements.txt
    
You are now set to run this code.
